#include "bitmapinfoheader.h"


unsigned short int BITMAPINFOHEADER::GETbiPlanes() {
    return biPlanes;
}

unsigned int BITMAPINFOHEADER::GETbiClrUsed() {
    return biClrUsed;
}




int  BITMAPINFOHEADER::GETbiWS(){
    return biWS;
}
int  BITMAPINFOHEADER::GETbiHS(){
    return biHS;
}
unsigned short int BITMAPINFOHEADER::GETbiBitCount(){
    return biBitCount;
}
int  BITMAPINFOHEADER::ReadBmpInfoHeader (ifstream &fp) {
    fp.read ((char*)&biSize, sizeof(biSize));
    fp.read ((char*)&biWS, sizeof(biWS));
    fp.read ((char*)&biHS, sizeof(biHS));
    fp.read ((char*)&biPlanes, sizeof(biPlanes));
    fp.read ((char*)&biBitCount, sizeof(biBitCount));
    fp.read ((char*)&biCompression, sizeof(biCompression));
    fp.read ((char*)&biSZ, sizeof(biSZ));
    fp.read ((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
    fp.read ((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
    fp.read ((char*)&biClrUsed, sizeof(biClrUsed));
    fp.read ((char*)&biClrImportant,  sizeof(biClrImportant));

    return 1;
}



int  BITMAPINFOHEADER::WriteBmpInfoHeader (ofstream &fp){
    fp.write ((char*)&biSize, sizeof(biSize));
    fp.write ((char*)&biWS, sizeof(biWS));
    fp.write ((char*)&biHS, sizeof(biHS));
    fp.write ((char*)&biPlanes, sizeof(biPlanes));
    fp.write ((char*)&biBitCount, sizeof(biBitCount));
    fp.write ((char*)&biCompression, sizeof(biCompression));
    fp.write ((char*)&biSZ, sizeof(biSZ));
    fp.write ((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
    fp.write ((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
    fp.write ((char*)&biClrUsed, sizeof(biClrUsed));
    fp.write ((char*)&biClrImportant,  sizeof(biClrImportant));
    return 1;
}
