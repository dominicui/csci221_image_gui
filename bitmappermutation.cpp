#include "bitmappermutation.h"
#include "bitmapfileheader.h"
#include "bitmapinfoheader.h"


BITMAPPERMUTATION::BITMAPPERMUTATION()
{

}

int BITMAPPERMUTATION::myrandom(int i) {
    return std::rand()%i;
}


int BITMAPPERMUTATION::myMain(string input_file, string output_file){


    BITMAPFILEHEADER bfh;
    BITMAPINFOHEADER bhd;



    ifstream fp1 (input_file, ios::in|ios::binary);
    if (!fp1.is_open()){
        //cout << "Usage: " << argv[0] << " <input_filename> <output_filename>"<< endl;
        cout << "Usage: " << input_file << " <input_filename> <output_filename>"<< endl;
        return 1;
    }


    ofstream fp2 (output_file, ios::out|ios::binary);
    if (!fp2.is_open()){
        cout << "Usage: " << output_file << " <input_filename> <output_filename>"<< endl;
        return 1;
    }


    int  success = 0;
    success = bfh.ReadBmpFileHeader(fp1) ;
    if (!success)
    {
        /* Couldn't read the file header - return NULL... */
        fp1.close();
        return -1;
    }


    if (bfh.GETbfType() != BF_TYPE)  /* Check for BM reversed, ie MB... */
    {
        cout << "ID is: " <<  bfh.GETbfType() << " Should have been" << 'M'*256+'B';
        cout <<  bfh.GETbfType()/256 << " " <<  bfh.GETbfType()%256 << endl;
        /* Not a bitmap file - return NULL... */
        fp1.close();
        return 1;
    }


    cout << "Image data Size: " << bfh.GETbfSize() << endl;

    success = 0;
    success = bhd.ReadBmpInfoHeader(fp1);
    if (!success)
    {
        /* Couldn't read the file header - return NULL... */
        //fp2.close();
        return -1;
    }

    cout << "Image Width Size:  " << bhd.GETbiWS() << endl;
    cout << "Image Height Size:  " << bhd.GETbiHS() << endl;
    cout<< "Bitcount: " << bhd.GETbiBitCount() << endl;
    cout<< "Color Planes " << bhd.GETbiClrUsed()<< endl;
    cout<< "Please wait, this operation takes some time ..." <<endl;


    //beginn of implementation


    std::vector <int> row_red;
    std::vector <int> row_green;
    std::vector <int> row_blue;

    std::vector <int> column_red;
    std::vector <int> column_green;
    std::vector <int> column_blue;

    unsigned char row_red_pixel;
    unsigned char row_green_pixel;
    unsigned char row_blue_pixel;


    //fill the row map in the right order
    int counter = 0;
    for (int i=0 ; i < bhd.GETbiHS(); i++) {
            for (int j=0 ; j < bhd.GETbiWS(); j++) {

                fp1.read ((char*)&row_red_pixel, 1);
                fp1.read ((char*)&row_green_pixel, 1);
                fp1.read ((char*)&row_blue_pixel, 1);

                row_red.push_back((int)row_red_pixel);
                row_green.push_back((int)row_green_pixel);
                row_blue.push_back((int)row_blue_pixel);

                counter++;

            }
    }


    fp1.close ();


//generate and calculate permutation keys
bool column_key_prime;
bool row_key_prime;
int row_offset;
int column_offset;

int width = bhd.GETbiWS();
int height = bhd.GETbiHS();
int column_key_length;
int row_key_length;


for(int a=width; a>3; a--) {

    if(width%a == 0) {
        row_key_length = a;
        row_key_prime = false;
        break;

    }

    /*else if(width%a != 0 && width%a<2) {
        row_key_length = a;
        row_key_prime = true;
        row_offset = width%a;

    }
    */

}

for(int b=height; b>3; b--) {

    if(height%b == 0) {
        column_key_length = b;
        column_key_prime = false;
        break;
    }
    /*
    else if(height%b != 0 && width%b<2) {
            column_key_length = b;
            column_key_prime = true;
            column_offset = height%b;
        }
        */
}


//generate random keys
std::vector<int> column_r_key;
std::vector<int> column_g_key;
std::vector<int> column_b_key;

std::vector<int> row_r_key;
std::vector<int> row_g_key;
std::vector<int> row_b_key;


std::vector<int> temp_column;
for (int i=0; i<column_key_length; i++) {

    temp_column.push_back(i);

}


std::vector<int> temp_row;
for (int ix=0; ix<row_key_length; ix++) {
    temp_row.push_back(ix);

}



column_r_key = temp_column;
column_g_key = temp_column;
column_b_key = temp_column;

row_r_key = temp_row;
row_g_key = temp_row;
row_b_key = temp_row;


unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

std::shuffle ( column_r_key.begin(), column_r_key.end(), std::default_random_engine(seed));
std::shuffle ( column_g_key.begin(), column_g_key.end(), std::default_random_engine(seed));
std::shuffle ( column_b_key.begin(), column_b_key.end(), std::default_random_engine(seed) );

std::shuffle ( row_r_key.begin(), row_r_key.end(), std::default_random_engine(seed) );
std::shuffle ( row_g_key.begin(), row_g_key.end(), std::default_random_engine(seed) );
std::shuffle ( row_b_key.begin(), row_b_key.end(), std::default_random_engine(seed) );

std::random_shuffle ( column_r_key.begin(), column_r_key.end());
std::random_shuffle ( column_g_key.begin(), column_g_key.end());
std::random_shuffle ( column_b_key.begin(), column_b_key.end());

std::random_shuffle ( row_r_key.begin(), row_r_key.end());
std::random_shuffle ( row_g_key.begin(), row_g_key.end());
std::random_shuffle ( row_b_key.begin(), row_b_key.end());

/*
cout<<"lengthRowKey "<<row_key_length<<endl;

for(int axi=0; axi<row_key_length; axi++) {
    cout<<row_r_key.at(axi);

}
cout<<endl;
for(int bxi=0; bxi<row_key_length; bxi++) {
    cout<<row_g_key.at(bxi);

}
cout<<endl;
for(int cxi=0; cxi<row_key_length; cxi++) {
    cout<<row_b_key.at(cxi);

}

cout<<endl;
cout<<"lengthColumnKey "<<column_key_length<<endl;

for(int dxi=0; dxi<column_key_length; dxi++) {
    cout<<column_r_key.at(dxi);
}
cout<<endl;
for(int exi=0; exi<column_key_length; exi++) {
    cout<<column_g_key.at(exi);
}
cout<<endl;
for(int fxi=0; fxi<column_key_length; fxi++) {
    cout<<column_b_key.at(fxi);
}
*/


//swap the column and row RGB maps with the generated Permutation and save it in new maps
std::vector <int> enc_column_red;
std::vector <int> enc_column_green;
std::vector <int> enc_column_blue;

std::vector <int> enc_row_red;
std::vector <int> enc_row_green;
std::vector <int> enc_row_blue;


//swap row position of map with the permutation
for(int o= 0; o<height; o++) {

    for(int p=0; p<width/row_key_length; p++) {

        for(int z=0; z<row_key_length; z++) {

            int temp = (o*width)+(p*row_key_length);

            int key_r = row_r_key.at(z);
            int key_g = row_g_key.at(z);
            int key_b = row_b_key.at(z);

            int value_r = row_red.at(key_r+temp);
            int value_g = row_green.at(key_g+temp);
            int value_b = row_blue.at(key_b+temp);

            enc_row_red.push_back(value_r);
            enc_row_green.push_back(value_g);
            enc_row_blue.push_back(value_b);

        }
    }
    /*
    if(row_key_prime == true) {
        recent_map_key++;
        for(int i=0; i<row_offset; i++) {
            enc_row_red.insert(std::pair<int,int>(recent_map_key, row_red.at(recent_map_key)));
            enc_row_green.insert(std::pair<int,int>(recent_map_key, row_green.at(recent_map_key)));
            enc_row_blue.insert(std::pair<int,int>(recent_map_key, row_blue.at(recent_map_key)));
            recent_map_key++;
        }

        }
*/
}


/*
cout<<endl;
for(int ibc=0; ibc<8; ibc++) {
    cout<<row_red.at(ibc);
    cout<<" ";

}
cout<<endl;
for(int ixa=0; ixa<8; ixa++) {

    cout<<enc_row_red.at(ixa);
    cout<<" ";
}
*/

//fill the column map in the right order
for(int a= 0; a<width; a++) {
        for (int i=0 ; i < width*height; i= i+width) {

                column_red.push_back(enc_row_red.at(i+a));
                column_green.push_back(enc_row_green.at(i+a));
                column_blue.push_back(enc_row_blue.at(i+a));

    }
}


for(int o= 0; o<width; o++) {

    for(int p=0; p<height/column_key_length; p++) {

        for(int z=0; z<column_key_length; z++) {

            int temp = (o*height)+(p*column_key_length);

            int key_r = column_r_key.at(z);
            int key_g = column_g_key.at(z);
            int key_b = column_b_key.at(z);

            int value_r = column_red.at(key_r+temp);
            int value_g = column_green.at(key_g+temp);
            int value_b = column_blue.at(key_b+temp);

            enc_column_red.push_back(value_r);
            enc_column_green.push_back(value_g);
            enc_column_blue.push_back(value_b);

        }
    }
    }



//output vectors
std::vector <int> out_red;
std::vector <int> out_green;
std::vector <int> out_blue;


for(int a= 0; a<height; a++) {
        for (int in=0 ; in < width*height; in = in+height) {

                out_red.push_back(enc_column_red.at(in+a));
                out_green.push_back(enc_column_green.at(in+a));
                out_blue.push_back(enc_column_blue.at(in+a));

    }
}



    bfh.WriteBmpFileHeader(fp2);
    bhd.WriteBmpInfoHeader(fp2);
        unsigned char enc_r, enc_g, enc_b;
        for (int ix=0 ; ix < bhd.GETbiHS(); ix++){
                for (int jx=0 ; jx < bhd.GETbiWS(); jx++) {


                    enc_r = out_red.at(jx+(ix*bhd.GETbiHS()));
                    enc_g = out_green.at(jx+(ix*bhd.GETbiHS()));
                    enc_b = out_blue.at(jx+(ix*bhd.GETbiHS()));

                    fp2.write ((char*)&enc_r, sizeof(char));
                    fp2.write ((char*)&enc_g, sizeof(char));
                    fp2.write ((char*)&enc_b, sizeof(char));

                }
        }


        fp2.close ();


        string temp = ".txt";
        ofstream outputFile(input_file+temp);

        outputFile << input_file;
        outputFile<<"\n";
        outputFile <<output_file;
        outputFile<<"\n";
        cout << "The Histogram data is written to "<< input_file << ".txt"<<endl;

        //outputFile << "Histogram data for R channel:\n";


        outputFile << "RR";
        for(int i=0; i<row_r_key.size(); i++) {
            outputFile <<row_r_key.at(i);
        }

        outputFile<<"\n";

        outputFile << "RG";
        for(int i=0; i<row_g_key.size(); i++) {
            outputFile <<row_g_key.at(i);
                }
        outputFile<<"\n";

        outputFile << "RB";
        for(int i=0; i<row_b_key.size(); i++) {
            outputFile <<row_b_key.at(i);
                        }
        outputFile<<"\n";

        outputFile << "CR";
        for(int i=0; i<column_r_key.size(); i++) {
                    outputFile <<column_r_key.at(i);
                                }
        outputFile<<"\n";

            outputFile << "CG";
        for(int i=0; i<column_g_key.size(); i++) {
            outputFile <<column_g_key.at(i);
                                        }
        outputFile<<"\n";

        outputFile << "CB";
        for(int i=0; i<column_b_key.size(); i++) {
            outputFile <<column_b_key.at(i);
                                                }
        outputFile<<"\n";

        return 0;
}
