#include "bitmapfileheader.h"



unsigned short BITMAPFILEHEADER::GETbfType(){
    return bfType;
}

unsigned long int   BITMAPFILEHEADER::GETbfSize(){
    return bfSize;
}


int  BITMAPFILEHEADER::ReadBmpFileHeader (ifstream &fp){
    //read buffer, length

    fp.read ((char*)&bfType, sizeof(bfType));
    fp.read ((char*)&bfSize, sizeof(bfSize));
    fp.read ((char*)&bfReserved1, sizeof(bfReserved1));
    fp.read ((char*)&bfReserved2, sizeof(bfReserved2));
    fp.read ((char*)&bfOffBits, sizeof(bfOffBits));

    return 1;
}

int  BITMAPFILEHEADER::WriteBmpFileHeader (ofstream &fp){
    fp.write ((char*)&bfType, sizeof(bfType));
    fp.write ((char*)&bfSize, sizeof(bfSize));
    fp.write ((char*)&bfReserved1, sizeof(bfReserved1));
    fp.write ((char*)&bfReserved2, sizeof(bfReserved2));
    fp.write ((char*)&bfOffBits, sizeof(bfOffBits));
    return 1;
}
