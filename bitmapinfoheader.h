#ifndef BITMAPINFOHEADER_H
#define BITMAPINFOHEADER_H
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
#include <map>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

class  BITMAPINFOHEADER      /**** BMP file info structure ****/
{
public:
    unsigned int   biSize;            /* Size of info header */
    int            biWS;              /* Width of image */
    int            biHS;              /* Height of image */
    unsigned short int biPlanes;      /* Number of color planes */
    unsigned short int biBitCount;    /* Number of bits per pixel */
    unsigned int   biCompression;     /* Type of compression to use */
    unsigned int   biSZ;              /* Size of image data */
    int            biXPelsPerMeter;   /* X pixels per meter */
    int            biYPelsPerMeter;   /* Y pixels per meter */
    unsigned int   biClrUsed;         /* Number of colors used */
    unsigned int   biClrImportant;    /* Number of important colors */
public:
    int            GETbiWS();
    int            GETbiHS();
    unsigned short int GETbiBitCount();
    int  ReadBmpInfoHeader (ifstream &);
    int  WriteBmpInfoHeader (ofstream &);

    unsigned short int GETbiPlanes();
    unsigned int GETbiClrUsed();
};


#endif // BITMAPINFOHEADER_H
