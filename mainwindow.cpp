#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <string>

QString inputFile;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{

     ui->setupUi(this);
     enc = new BITMAPPERMUTATION();
     ui->label->setStyleSheet("QLabel {background-color: white; color:black}");
     ui->label_2->setStyleSheet("QLabel {background-color: white; color:black}");
     ui->label_3->setStyleSheet("QLabel {background-color: white; color:black}");
    // connect(ui->pushButton, SIGNAL(clicked()), ui->pushButton_2, SLOT(setEnabled(TRUE)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    QFileDialog dialog(this);
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Images"), "/", tr("Image Files (*.bmp)"));

    inputFile = fileName;

    int w = ui->label->width();
    int h = ui->label->height();

    if(!fileName.isEmpty()){
        QImage image(fileName);
        ui->label->setPixmap(QPixmap::fromImage(image.scaled(w,h)));
    }

}


void MainWindow::on_pushButton_2_clicked()
{

    //QFileDialog dialog(this);
    //QString fileName = QFileDialog::getOpenFileName(this, tr("Open Images"), "/", tr("Image Files (*.bmp)"));

    int w = ui->label_2->width();
    int h = ui->label_2->height();

        //Retrieves fileName in order to output encrypted image
        string input_file = inputFile.toLocal8Bit().constData();
        string output_file(input_file + ".out.bmp");
        enc->myMain(input_file, output_file);
        QString qStrOutput_file = QString::fromStdString(output_file);
        QImage image2(qStrOutput_file);
        ui->label_2->setPixmap(QPixmap::fromImage(image2.scaled(w,h)));
}


void MainWindow::on_pushButton_3_clicked()
{

    int w = ui->label_3->width();
    int h = ui->label_3->height();


        QImage image3("lena.bmp");
        ui->label_3->setPixmap(QPixmap::fromImage(image3.scaled(w,h)));


}
