#ifndef BITMAPFILEHEADER_H
#define BITMAPFILEHEADER_H
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
#include <map>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

class BITMAPFILEHEADER               /**** BMP file header structure ****/
{
private:
    unsigned short bfType;           /* Magic number for file */
    unsigned int   bfSize;           /* Size of file */
    unsigned short bfReserved1;      /* Reserved */
    unsigned short bfReserved2;      /* ... */
    unsigned int   bfOffBits;        /* Offset to bitmap data */
public:
    unsigned short GETbfType();
    unsigned long int   GETbfSize();
    int  ReadBmpFileHeader (ifstream &);
    int  WriteBmpFileHeader (ofstream &);

};


#endif // BITMAPFILEHEADER_H

