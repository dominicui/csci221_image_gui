#ifndef BITMAPPERMUTATION_H
#define BITMAPPERMUTATION_H
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
#include <map>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

#  define BF_TYPE 0x4D42                   /* "MB" */


class BITMAPPERMUTATION
{
public:
    BITMAPPERMUTATION();
    int myrandom(int i);
    int myMain(string input_file, string output_file);

};

#endif // BITMAPPERMUTATION_H
